package com.gaus;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements OnClickListener {

	public static int m=0;
	public static int n=0;
	public static boolean exit;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//if(exit)finish();

		Button b1 = (Button)findViewById(R.id.button1);
		Button b2 = (Button)findViewById(R.id.button2);
		Button b3 = (Button)findViewById(R.id.button3);
		Button b4 = (Button)findViewById(R.id.button4);
		Button b5 = (Button)findViewById(R.id.button5);
		Button b6 = (Button)findViewById(R.id.button6);
		Button b7 = (Button)findViewById(R.id.button7);
		Button b8 = (Button)findViewById(R.id.button8);
		Button b9 = (Button)findViewById(R.id.button9);
		Button b10 = (Button)findViewById(R.id.button10);
		Button b11 = (Button)findViewById(R.id.button11);
		Button b12 = (Button)findViewById(R.id.button12);
		Button b13 = (Button)findViewById(R.id.button13);
		Button b14 = (Button)findViewById(R.id.button14);
		Button b15 = (Button)findViewById(R.id.button15);
		Button b16 = (Button)findViewById(R.id.button16);
		Button b17 = (Button)findViewById(R.id.button17);
		Button b18 = (Button)findViewById(R.id.button18);




		b1.setOnClickListener(this);
		b2.setOnClickListener(this);
		b3.setOnClickListener(this);
		b4.setOnClickListener(this);
		b5.setOnClickListener(this);
		b6.setOnClickListener(this);
		b7.setOnClickListener(this);
		b8.setOnClickListener(this);
		b9.setOnClickListener(this);
		b10.setOnClickListener(this);
		b11.setOnClickListener(this);
		b12.setOnClickListener(this);
		b13.setOnClickListener(this);
		b14.setOnClickListener(this);
		b15.setOnClickListener(this);
		b16.setOnClickListener(this);
		b17.setOnClickListener(this);
		b18.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if(v.getId()==findViewById(R.id.button1).getId())m=1;
		if(v.getId()==findViewById(R.id.button2).getId())m=2;
		if(v.getId()==findViewById(R.id.button3).getId())m=3;
		if(v.getId()==findViewById(R.id.button4).getId())m=4;
		if(v.getId()==findViewById(R.id.button5).getId())m=5;
		if(v.getId()==findViewById(R.id.button6).getId())m=6;
		if(v.getId()==findViewById(R.id.button7).getId())m=7;
		if(v.getId()==findViewById(R.id.button8).getId())m=8;
		if(v.getId()==findViewById(R.id.button9).getId())m=9;

		if(m!=0){

			TextView text_m=(TextView) findViewById(R.id.textView1);
			text_m.setText("Number of lines (m = "+m+"):");	
		}

		if(v.getId()==findViewById(R.id.button10).getId())n=1;
		if(v.getId()==findViewById(R.id.button11).getId())n=2;
		if(v.getId()==findViewById(R.id.button12).getId())n=3;
		if(v.getId()==findViewById(R.id.button13).getId())n=4;
		if(v.getId()==findViewById(R.id.button14).getId())n=5;
		if(v.getId()==findViewById(R.id.button15).getId())n=6;
		if(v.getId()==findViewById(R.id.button16).getId())n=7;
		if(v.getId()==findViewById(R.id.button17).getId())n=8;
		if(v.getId()==findViewById(R.id.button18).getId())n=9;

		if(n!=0){

			TextView text_n=(TextView) findViewById(R.id.textView2);
			text_n.setText("Number of columns (n = "+n+"):");

		}

		if(n!=0 && m!=0){

			Intent hurren= new Intent(this, MainActivity2.class);
			startActivity(hurren);
		}


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			
			System.exit(1);
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		
		System.exit(1);
	}
}
