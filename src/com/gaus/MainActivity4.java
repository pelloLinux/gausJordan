package com.gaus;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity4 extends ActionBarActivity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_activity4);

		TextView t11=(TextView) findViewById(R.id.editText1);
		TextView t21=(TextView) findViewById(R.id.EditText01);
		TextView t31=(TextView) findViewById(R.id.EditText03);
		TextView t41=(TextView) findViewById(R.id.EditText04);
		TextView t51=(TextView) findViewById(R.id.EditText05);
		TextView t61=(TextView) findViewById(R.id.EditText06);
		TextView t71=(TextView) findViewById(R.id.EditText07);
		TextView t81=(TextView) findViewById(R.id.EditText08);
		TextView t91=(TextView) findViewById(R.id.editText68);

		TextView t12=(TextView) findViewById(R.id.editText2);
		TextView t22=(TextView) findViewById(R.id.editText3);
		TextView t32=(TextView) findViewById(R.id.editText4);
		TextView t42=(TextView) findViewById(R.id.editText5);
		TextView t52=(TextView) findViewById(R.id.editText7);
		TextView t62=(TextView) findViewById(R.id.editText8);
		TextView t72=(TextView) findViewById(R.id.editText6);
		TextView t82=(TextView) findViewById(R.id.editText9);
		TextView t92=(TextView) findViewById(R.id.editText69);

		TextView t13=(TextView) findViewById(R.id.editText10);
		TextView t23=(TextView) findViewById(R.id.editText11);
		TextView t33=(TextView) findViewById(R.id.editText12);
		TextView t43=(TextView) findViewById(R.id.editText14);
		TextView t53=(TextView) findViewById(R.id.editText15);
		TextView t63=(TextView) findViewById(R.id.editText13);
		TextView t73=(TextView) findViewById(R.id.editText16);
		TextView t83=(TextView) findViewById(R.id.editText17);
		TextView t93=(TextView) findViewById(R.id.editText70);

		TextView t14=(TextView) findViewById(R.id.editText18);
		TextView t24=(TextView) findViewById(R.id.editText19);
		TextView t34=(TextView) findViewById(R.id.editText21);
		TextView t44=(TextView) findViewById(R.id.editText22);
		TextView t54=(TextView) findViewById(R.id.editText20);
		TextView t64=(TextView) findViewById(R.id.editText23);
		TextView t74=(TextView) findViewById(R.id.editText24);
		TextView t84=(TextView) findViewById(R.id.editText25);
		TextView t94=(TextView) findViewById(R.id.editText71);

		TextView t15=(TextView) findViewById(R.id.editText27);
		TextView t25=(TextView) findViewById(R.id.editText28);
		TextView t35=(TextView) findViewById(R.id.editText29);
		TextView t45=(TextView) findViewById(R.id.editText31);
		TextView t55=(TextView) findViewById(R.id.editText32);
		TextView t65=(TextView) findViewById(R.id.editText33);
		TextView t75=(TextView) findViewById(R.id.editText34);
		TextView t85=(TextView) findViewById(R.id.editText35);
		TextView t95=(TextView) findViewById(R.id.editText72);

		TextView t16=(TextView) findViewById(R.id.editText36);
		TextView t26=(TextView) findViewById(R.id.editText37);
		TextView t36=(TextView) findViewById(R.id.editText38);
		TextView t46=(TextView) findViewById(R.id.editText39);
		TextView t56=(TextView) findViewById(R.id.editText40);
		TextView t66=(TextView) findViewById(R.id.editText41);
		TextView t76=(TextView) findViewById(R.id.editText42);
		TextView t86=(TextView) findViewById(R.id.editText43);
		TextView t96=(TextView) findViewById(R.id.editText73);

		TextView t17=(TextView) findViewById(R.id.editText44);
		TextView t27=(TextView) findViewById(R.id.editText45);
		TextView t37=(TextView) findViewById(R.id.editText46);
		TextView t47=(TextView) findViewById(R.id.editText47);
		TextView t57=(TextView) findViewById(R.id.editText48);
		TextView t67=(TextView) findViewById(R.id.editText49);
		TextView t77=(TextView) findViewById(R.id.editText50);
		TextView t87=(TextView) findViewById(R.id.editText51);
		TextView t97=(TextView) findViewById(R.id.editText74);

		TextView t18=(TextView) findViewById(R.id.editText52);
		TextView t28=(TextView) findViewById(R.id.editText53);
		TextView t38=(TextView) findViewById(R.id.editText54);
		TextView t48=(TextView) findViewById(R.id.editText55);
		TextView t58=(TextView) findViewById(R.id.editText56);
		TextView t68=(TextView) findViewById(R.id.editText57);
		TextView t78=(TextView) findViewById(R.id.editText58);
		TextView t88=(TextView) findViewById(R.id.editText59);
		TextView t98=(TextView) findViewById(R.id.editText75);

		TextView t19=(TextView) findViewById(R.id.editText60);
		TextView t29=(TextView) findViewById(R.id.editText61);
		TextView t39=(TextView) findViewById(R.id.editText62);
		TextView t49=(TextView) findViewById(R.id.editText63);
		TextView t59=(TextView) findViewById(R.id.editText64);
		TextView t69=(TextView) findViewById(R.id.editText65);
		TextView t79=(TextView) findViewById(R.id.editText66);
		TextView t89=(TextView) findViewById(R.id.editText67);
		TextView t99=(TextView) findViewById(R.id.editText76);


		if(MainActivity.m<9){

			t91.setVisibility(View.INVISIBLE);
			t92.setVisibility(View.INVISIBLE);
			t93.setVisibility(View.INVISIBLE);
			t94.setVisibility(View.INVISIBLE);
			t95.setVisibility(View.INVISIBLE);
			t96.setVisibility(View.INVISIBLE);
			t97.setVisibility(View.INVISIBLE);
			t98.setVisibility(View.INVISIBLE);
			t99.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<8){

			t81.setVisibility(View.INVISIBLE);
			t82.setVisibility(View.INVISIBLE);
			t83.setVisibility(View.INVISIBLE);
			t84.setVisibility(View.INVISIBLE);
			t85.setVisibility(View.INVISIBLE);
			t86.setVisibility(View.INVISIBLE);
			t87.setVisibility(View.INVISIBLE);
			t88.setVisibility(View.INVISIBLE);
			t89.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<7){

			t71.setVisibility(View.INVISIBLE);
			t72.setVisibility(View.INVISIBLE);
			t73.setVisibility(View.INVISIBLE);
			t74.setVisibility(View.INVISIBLE);
			t75.setVisibility(View.INVISIBLE);
			t76.setVisibility(View.INVISIBLE);
			t77.setVisibility(View.INVISIBLE);
			t78.setVisibility(View.INVISIBLE);
			t79.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<6){

			t61.setVisibility(View.INVISIBLE);
			t62.setVisibility(View.INVISIBLE);
			t63.setVisibility(View.INVISIBLE);
			t64.setVisibility(View.INVISIBLE);
			t65.setVisibility(View.INVISIBLE);
			t66.setVisibility(View.INVISIBLE);
			t67.setVisibility(View.INVISIBLE);
			t68.setVisibility(View.INVISIBLE);
			t69.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<5){

			t51.setVisibility(View.INVISIBLE);
			t52.setVisibility(View.INVISIBLE);
			t53.setVisibility(View.INVISIBLE);
			t54.setVisibility(View.INVISIBLE);
			t55.setVisibility(View.INVISIBLE);
			t56.setVisibility(View.INVISIBLE);
			t57.setVisibility(View.INVISIBLE);
			t58.setVisibility(View.INVISIBLE);
			t59.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<4){

			t41.setVisibility(View.INVISIBLE);
			t42.setVisibility(View.INVISIBLE);
			t43.setVisibility(View.INVISIBLE);
			t44.setVisibility(View.INVISIBLE);
			t45.setVisibility(View.INVISIBLE);
			t46.setVisibility(View.INVISIBLE);
			t47.setVisibility(View.INVISIBLE);
			t48.setVisibility(View.INVISIBLE);
			t49.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<3){

			t31.setVisibility(View.INVISIBLE);
			t32.setVisibility(View.INVISIBLE);
			t33.setVisibility(View.INVISIBLE);
			t34.setVisibility(View.INVISIBLE);
			t35.setVisibility(View.INVISIBLE);
			t36.setVisibility(View.INVISIBLE);
			t37.setVisibility(View.INVISIBLE);
			t38.setVisibility(View.INVISIBLE);
			t39.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<2){

			t21.setVisibility(View.INVISIBLE);
			t22.setVisibility(View.INVISIBLE);
			t23.setVisibility(View.INVISIBLE);
			t24.setVisibility(View.INVISIBLE);
			t25.setVisibility(View.INVISIBLE);
			t26.setVisibility(View.INVISIBLE);
			t27.setVisibility(View.INVISIBLE);
			t28.setVisibility(View.INVISIBLE);
			t29.setVisibility(View.INVISIBLE);

		}

		if(MainActivity.n<9){

			t19.setVisibility(View.INVISIBLE);
			t29.setVisibility(View.INVISIBLE);
			t39.setVisibility(View.INVISIBLE);
			t49.setVisibility(View.INVISIBLE);
			t59.setVisibility(View.INVISIBLE);
			t69.setVisibility(View.INVISIBLE);
			t79.setVisibility(View.INVISIBLE);
			t89.setVisibility(View.INVISIBLE);
			t99.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<8){

			t18.setVisibility(View.INVISIBLE);
			t28.setVisibility(View.INVISIBLE);
			t38.setVisibility(View.INVISIBLE);
			t48.setVisibility(View.INVISIBLE);
			t58.setVisibility(View.INVISIBLE);
			t68.setVisibility(View.INVISIBLE);
			t78.setVisibility(View.INVISIBLE);
			t88.setVisibility(View.INVISIBLE);
			t98.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<7){

			t17.setVisibility(View.INVISIBLE);
			t27.setVisibility(View.INVISIBLE);
			t37.setVisibility(View.INVISIBLE);
			t47.setVisibility(View.INVISIBLE);
			t57.setVisibility(View.INVISIBLE);
			t67.setVisibility(View.INVISIBLE);
			t77.setVisibility(View.INVISIBLE);
			t87.setVisibility(View.INVISIBLE);
			t97.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<6){

			t16.setVisibility(View.INVISIBLE);
			t26.setVisibility(View.INVISIBLE);
			t36.setVisibility(View.INVISIBLE);
			t46.setVisibility(View.INVISIBLE);
			t56.setVisibility(View.INVISIBLE);
			t66.setVisibility(View.INVISIBLE);
			t76.setVisibility(View.INVISIBLE);
			t86.setVisibility(View.INVISIBLE);
			t96.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<5){

			t15.setVisibility(View.INVISIBLE);
			t25.setVisibility(View.INVISIBLE);
			t35.setVisibility(View.INVISIBLE);
			t45.setVisibility(View.INVISIBLE);
			t55.setVisibility(View.INVISIBLE);
			t65.setVisibility(View.INVISIBLE);
			t75.setVisibility(View.INVISIBLE);
			t85.setVisibility(View.INVISIBLE);
			t95.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<4){

			t14.setVisibility(View.INVISIBLE);
			t24.setVisibility(View.INVISIBLE);
			t34.setVisibility(View.INVISIBLE);
			t44.setVisibility(View.INVISIBLE);
			t54.setVisibility(View.INVISIBLE);
			t64.setVisibility(View.INVISIBLE);
			t74.setVisibility(View.INVISIBLE);
			t84.setVisibility(View.INVISIBLE);
			t94.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<3){

			t13.setVisibility(View.INVISIBLE);
			t23.setVisibility(View.INVISIBLE);
			t33.setVisibility(View.INVISIBLE);
			t43.setVisibility(View.INVISIBLE);
			t53.setVisibility(View.INVISIBLE);
			t63.setVisibility(View.INVISIBLE);
			t73.setVisibility(View.INVISIBLE);
			t83.setVisibility(View.INVISIBLE);
			t93.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<2){

			t12.setVisibility(View.INVISIBLE);
			t22.setVisibility(View.INVISIBLE);
			t32.setVisibility(View.INVISIBLE);
			t42.setVisibility(View.INVISIBLE);
			t52.setVisibility(View.INVISIBLE);
			t62.setVisibility(View.INVISIBLE);
			t72.setVisibility(View.INVISIBLE);
			t82.setVisibility(View.INVISIBLE);
			t92.setVisibility(View.INVISIBLE);

		}

		setMatrix(MainActivity3.lista2.get(MainActivity3.index));

		ImageButton ezker = (ImageButton)findViewById(R.id.imageButton1);
		ImageButton eskuin = (ImageButton)findViewById(R.id.imageButton2);
		Button exit = (Button)findViewById(R.id.button1);

		ezker.setOnClickListener(this);
		eskuin.setOnClickListener(this);
		exit.setOnClickListener(this);
	}

	private void setMatrix(float[][] m){

		TextView t11=(TextView) findViewById(R.id.editText1);
		TextView t21=(TextView) findViewById(R.id.EditText01);
		TextView t31=(TextView) findViewById(R.id.EditText03);
		TextView t41=(TextView) findViewById(R.id.EditText04);
		TextView t51=(TextView) findViewById(R.id.EditText05);
		TextView t61=(TextView) findViewById(R.id.EditText06);
		TextView t71=(TextView) findViewById(R.id.EditText07);
		TextView t81=(TextView) findViewById(R.id.EditText08);
		TextView t91=(TextView) findViewById(R.id.editText68);

		TextView t12=(TextView) findViewById(R.id.editText2);
		TextView t22=(TextView) findViewById(R.id.editText3);
		TextView t32=(TextView) findViewById(R.id.editText4);
		TextView t42=(TextView) findViewById(R.id.editText5);
		TextView t52=(TextView) findViewById(R.id.editText7);
		TextView t62=(TextView) findViewById(R.id.editText8);
		TextView t72=(TextView) findViewById(R.id.editText6);
		TextView t82=(TextView) findViewById(R.id.editText9);
		TextView t92=(TextView) findViewById(R.id.editText69);

		TextView t13=(TextView) findViewById(R.id.editText10);
		TextView t23=(TextView) findViewById(R.id.editText11);
		TextView t33=(TextView) findViewById(R.id.editText12);
		TextView t43=(TextView) findViewById(R.id.editText14);
		TextView t53=(TextView) findViewById(R.id.editText15);
		TextView t63=(TextView) findViewById(R.id.editText13);
		TextView t73=(TextView) findViewById(R.id.editText16);
		TextView t83=(TextView) findViewById(R.id.editText17);
		TextView t93=(TextView) findViewById(R.id.editText70);

		TextView t14=(TextView) findViewById(R.id.editText18);
		TextView t24=(TextView) findViewById(R.id.editText19);
		TextView t34=(TextView) findViewById(R.id.editText21);
		TextView t44=(TextView) findViewById(R.id.editText22);
		TextView t54=(TextView) findViewById(R.id.editText20);
		TextView t64=(TextView) findViewById(R.id.editText23);
		TextView t74=(TextView) findViewById(R.id.editText24);
		TextView t84=(TextView) findViewById(R.id.editText25);
		TextView t94=(TextView) findViewById(R.id.editText71);

		TextView t15=(TextView) findViewById(R.id.editText27);
		TextView t25=(TextView) findViewById(R.id.editText28);
		TextView t35=(TextView) findViewById(R.id.editText29);
		TextView t45=(TextView) findViewById(R.id.editText31);
		TextView t55=(TextView) findViewById(R.id.editText32);
		TextView t65=(TextView) findViewById(R.id.editText33);
		TextView t75=(TextView) findViewById(R.id.editText34);
		TextView t85=(TextView) findViewById(R.id.editText35);
		TextView t95=(TextView) findViewById(R.id.editText72);

		TextView t16=(TextView) findViewById(R.id.editText36);
		TextView t26=(TextView) findViewById(R.id.editText37);
		TextView t36=(TextView) findViewById(R.id.editText38);
		TextView t46=(TextView) findViewById(R.id.editText39);
		TextView t56=(TextView) findViewById(R.id.editText40);
		TextView t66=(TextView) findViewById(R.id.editText41);
		TextView t76=(TextView) findViewById(R.id.editText42);
		TextView t86=(TextView) findViewById(R.id.editText43);
		TextView t96=(TextView) findViewById(R.id.editText73);

		TextView t17=(TextView) findViewById(R.id.editText44);
		TextView t27=(TextView) findViewById(R.id.editText45);
		TextView t37=(TextView) findViewById(R.id.editText46);
		TextView t47=(TextView) findViewById(R.id.editText47);
		TextView t57=(TextView) findViewById(R.id.editText48);
		TextView t67=(TextView) findViewById(R.id.editText49);
		TextView t77=(TextView) findViewById(R.id.editText50);
		TextView t87=(TextView) findViewById(R.id.editText51);
		TextView t97=(TextView) findViewById(R.id.editText74);

		TextView t18=(TextView) findViewById(R.id.editText52);
		TextView t28=(TextView) findViewById(R.id.editText53);
		TextView t38=(TextView) findViewById(R.id.editText54);
		TextView t48=(TextView) findViewById(R.id.editText55);
		TextView t58=(TextView) findViewById(R.id.editText56);
		TextView t68=(TextView) findViewById(R.id.editText57);
		TextView t78=(TextView) findViewById(R.id.editText58);
		TextView t88=(TextView) findViewById(R.id.editText59);
		TextView t98=(TextView) findViewById(R.id.editText75);

		TextView t19=(TextView) findViewById(R.id.editText60);
		TextView t29=(TextView) findViewById(R.id.editText61);
		TextView t39=(TextView) findViewById(R.id.editText62);
		TextView t49=(TextView) findViewById(R.id.editText63);
		TextView t59=(TextView) findViewById(R.id.editText64);
		TextView t69=(TextView) findViewById(R.id.editText65);
		TextView t79=(TextView) findViewById(R.id.editText66);
		TextView t89=(TextView) findViewById(R.id.editText67);
		TextView t99=(TextView) findViewById(R.id.editText76);

		String b="";

		float a = m[0][0];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t11.setText(b);


		a = m[0][1];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t12.setText(b);

		a = m[0][2];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t13.setText(b);

		a = m[0][3];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t14.setText(b);

		a = m[0][4];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t15.setText(b);

		a = m[0][5];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t16.setText(b);

		a = m[0][6];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t17.setText(b);

		a = m[0][7];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t18.setText(b);

		a = m[0][8];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t19.setText(b);

		//

		a = m[1][0];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t21.setText(b);

		a = m[1][1];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t22.setText(b);

		a = m[1][2];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t23.setText(b);

		a = m[1][3];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t24.setText(b);

		a = m[1][4];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t25.setText(b);

		a = m[1][5];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t26.setText(b);

		a = m[1][6];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t27.setText(b);

		a = m[1][7];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t28.setText(b);

		a = m[1][8];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t29.setText(b);

		//

		a = m[2][0];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t31.setText(b);

		a = m[2][1];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t32.setText(b);

		a = m[2][2];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t33.setText(b);

		a = m[2][3];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t34.setText(b);

		a = m[2][4];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t35.setText(b);

		a = m[2][5];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t36.setText(b);

		a = m[2][6];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t37.setText(b);

		a = m[2][7];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t38.setText(b);

		a = m[2][8];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t39.setText(b);


		//

		a = m[3][0];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t41.setText(b);

		a = m[3][1];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t42.setText(b);

		a = m[3][2];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t43.setText(b);

		a = m[3][3];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t44.setText(b);

		a = m[3][4];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t45.setText(b);

		a = m[3][5];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t46.setText(b);

		a = m[3][6];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t47.setText(b);

		a = m[3][7];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t48.setText(b);

		a = m[3][8];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t49.setText(b);

		//


		a = m[4][0];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t51.setText(b);

		a = m[4][1];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t52.setText(b);

		a = m[4][2];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t53.setText(b);

		a = m[4][3];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t54.setText(b);

		a = m[4][4];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t55.setText(b);

		a = m[4][5];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t56.setText(b);

		a = m[4][6];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t57.setText(b);

		a = m[4][7];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t58.setText(b);

		a = m[4][8];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t59.setText(b);

		//

		a = m[5][0];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t61.setText(b);

		a = m[5][1];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t62.setText(b);

		a = m[5][2];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t63.setText(b);

		a = m[5][3];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t64.setText(b);

		a = m[5][4];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t65.setText(b);

		a = m[5][5];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t66.setText(b);

		a = m[5][6];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t67.setText(b);

		a = m[5][7];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t68.setText(b);

		a = m[5][8];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t69.setText(b);


		//

		a = m[6][0];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t71.setText(b);

		a = m[6][1];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t72.setText(b);

		a = m[6][2];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t73.setText(b);

		a = m[6][3];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t74.setText(b);

		a = m[6][4];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t75.setText(b);

		a = m[6][5];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t76.setText(b);

		a = m[6][6];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t77.setText(b);

		a = m[6][7];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t78.setText(b);

		a = m[6][8];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t79.setText(b);


		//

		a = m[7][0];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t81.setText(b);

		a = m[7][1];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t82.setText(b);

		a = m[7][2];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t83.setText(b);

		a = m[7][3];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t84.setText(b);

		a = m[7][4];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t85.setText(b);

		a = m[7][5];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t86.setText(b);

		a = m[7][6];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t87.setText(b);

		a = m[7][7];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t88.setText(b);

		a = m[7][8];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t89.setText(b);

		//

		a = m[8][0];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t91.setText(b);

		a = m[8][1];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t92.setText(b);

		a = m[8][2];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t93.setText(b);

		a = m[8][3];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t94.setText(b);

		a = m[8][4];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t95.setText(b);

		a = m[8][5];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t96.setText(b);

		a = m[8][6];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t97.setText(b);

		a = m[8][7];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t98.setText(b);

		a = m[8][8];
		if(a%1!=0)
			b = metodoak1.zatikiraPasa(a);
		else b = String.valueOf((int)a);
		t99.setText(b);

	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub


		if(v.getId()==findViewById(R.id.imageButton1).getId()){

			MainActivity3.index--;
			if(MainActivity3.index>=0)setMatrix(MainActivity3.lista2.get(MainActivity3.index));
			else MainActivity3.index++;

		}
		if(v.getId()==findViewById(R.id.imageButton2).getId()){

			MainActivity3.index++;
			if(MainActivity3.index<=MainActivity3.lista2.size()-1)setMatrix(MainActivity3.lista2.get(MainActivity3.index));
			else MainActivity3.index--;

		}
		if(v.getId()==findViewById(R.id.button1).getId()){

			Intent hurren= new Intent(this, Main3Activity.class);
			startActivity(hurren);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_activity3, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			System.exit(1);
		}
		if (id == R.id.right) {

			MainActivity3.index++;
			if(MainActivity3.index<=MainActivity3.lista2.size()-1)setMatrix(MainActivity3.lista2.get(MainActivity3.index));
			else MainActivity3.index--;
		}
		if (id == R.id.left) {
			MainActivity3.index--;
			if(MainActivity3.index>=0)setMatrix(MainActivity3.lista2.get(MainActivity3.index));
			else MainActivity3.index++;
		}

		if(id == R.id.getNumber) {

			Intent hurren= new Intent(this, Main3Activity.class);
			startActivity(hurren);
		}
		return super.onOptionsItemSelected(item);
	}
}
