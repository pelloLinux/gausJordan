package com.gaus;
import java.util.LinkedList;

import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;


@SuppressLint("NewApi")
public class MainActivity2 extends ActionBarActivity implements OnClickListener {

	static float[][] matrix;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_activity2);

		EditText t11=(EditText)findViewById(R.id.editText1);
		EditText t21=(EditText)findViewById(R.id.EditText01);
		EditText t31=(EditText)findViewById(R.id.EditText03);
		EditText t41=(EditText)findViewById(R.id.EditText04);
		EditText t51=(EditText)findViewById(R.id.EditText05);
		EditText t61=(EditText)findViewById(R.id.EditText06);
		EditText t71=(EditText)findViewById(R.id.EditText07);
		EditText t81=(EditText)findViewById(R.id.EditText08);
		EditText t91=(EditText)findViewById(R.id.editText68);

		EditText t12=(EditText)findViewById(R.id.editText2);
		EditText t22=(EditText)findViewById(R.id.editText3);
		EditText t32=(EditText)findViewById(R.id.editText4);
		EditText t42=(EditText)findViewById(R.id.editText5);
		EditText t52=(EditText)findViewById(R.id.editText7);
		EditText t62=(EditText)findViewById(R.id.editText8);
		EditText t72=(EditText)findViewById(R.id.editText6);
		EditText t82=(EditText)findViewById(R.id.editText9);
		EditText t92=(EditText)findViewById(R.id.editText69);

		EditText t13=(EditText)findViewById(R.id.editText10);
		EditText t23=(EditText)findViewById(R.id.editText11);
		EditText t33=(EditText)findViewById(R.id.editText12);
		EditText t43=(EditText)findViewById(R.id.editText14);
		EditText t53=(EditText)findViewById(R.id.editText15);
		EditText t63=(EditText)findViewById(R.id.editText13);
		EditText t73=(EditText)findViewById(R.id.editText16);
		EditText t83=(EditText)findViewById(R.id.editText17);
		EditText t93=(EditText)findViewById(R.id.editText70);

		EditText t14=(EditText)findViewById(R.id.editText18);
		EditText t24=(EditText)findViewById(R.id.editText19);
		EditText t34=(EditText)findViewById(R.id.editText21);
		EditText t44=(EditText)findViewById(R.id.editText22);
		EditText t54=(EditText)findViewById(R.id.editText20);
		EditText t64=(EditText)findViewById(R.id.editText23);
		EditText t74=(EditText)findViewById(R.id.editText24);
		EditText t84=(EditText)findViewById(R.id.editText25);
		EditText t94=(EditText)findViewById(R.id.editText71);

		EditText t15=(EditText)findViewById(R.id.editText27);
		EditText t25=(EditText)findViewById(R.id.editText28);
		EditText t35=(EditText)findViewById(R.id.editText29);
		EditText t45=(EditText)findViewById(R.id.editText31);
		EditText t55=(EditText)findViewById(R.id.editText32);
		EditText t65=(EditText)findViewById(R.id.editText33);
		EditText t75=(EditText)findViewById(R.id.editText34);
		EditText t85=(EditText)findViewById(R.id.editText35);
		EditText t95=(EditText)findViewById(R.id.editText72);

		EditText t16=(EditText)findViewById(R.id.editText36);
		EditText t26=(EditText)findViewById(R.id.editText37);
		EditText t36=(EditText)findViewById(R.id.editText38);
		EditText t46=(EditText)findViewById(R.id.editText39);
		EditText t56=(EditText)findViewById(R.id.editText40);
		EditText t66=(EditText)findViewById(R.id.editText41);
		EditText t76=(EditText)findViewById(R.id.editText42);
		EditText t86=(EditText)findViewById(R.id.editText43);
		EditText t96=(EditText)findViewById(R.id.editText73);

		EditText t17=(EditText)findViewById(R.id.editText44);
		EditText t27=(EditText)findViewById(R.id.editText45);
		EditText t37=(EditText)findViewById(R.id.editText46);
		EditText t47=(EditText)findViewById(R.id.editText47);
		EditText t57=(EditText)findViewById(R.id.editText48);
		EditText t67=(EditText)findViewById(R.id.editText49);
		EditText t77=(EditText)findViewById(R.id.editText50);
		EditText t87=(EditText)findViewById(R.id.editText51);
		EditText t97=(EditText)findViewById(R.id.editText74);

		EditText t18=(EditText)findViewById(R.id.editText52);
		EditText t28=(EditText)findViewById(R.id.editText53);
		EditText t38=(EditText)findViewById(R.id.editText54);
		EditText t48=(EditText)findViewById(R.id.editText55);
		EditText t58=(EditText)findViewById(R.id.editText56);
		EditText t68=(EditText)findViewById(R.id.editText57);
		EditText t78=(EditText)findViewById(R.id.editText58);
		EditText t88=(EditText)findViewById(R.id.editText59);
		EditText t98=(EditText)findViewById(R.id.editText75);

		EditText t19=(EditText)findViewById(R.id.editText60);
		EditText t29=(EditText)findViewById(R.id.editText61);
		EditText t39=(EditText)findViewById(R.id.editText62);
		EditText t49=(EditText)findViewById(R.id.editText63);
		EditText t59=(EditText)findViewById(R.id.editText64);
		EditText t69=(EditText)findViewById(R.id.editText65);
		EditText t79=(EditText)findViewById(R.id.editText66);
		EditText t89=(EditText)findViewById(R.id.editText67);
		EditText t99=(EditText)findViewById(R.id.editText76);



		if(MainActivity.m<9){

			t91.setVisibility(View.INVISIBLE);
			t92.setVisibility(View.INVISIBLE);
			t93.setVisibility(View.INVISIBLE);
			t94.setVisibility(View.INVISIBLE);
			t95.setVisibility(View.INVISIBLE);
			t96.setVisibility(View.INVISIBLE);
			t97.setVisibility(View.INVISIBLE);
			t98.setVisibility(View.INVISIBLE);
			t99.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<8){

			t81.setVisibility(View.INVISIBLE);
			t82.setVisibility(View.INVISIBLE);
			t83.setVisibility(View.INVISIBLE);
			t84.setVisibility(View.INVISIBLE);
			t85.setVisibility(View.INVISIBLE);
			t86.setVisibility(View.INVISIBLE);
			t87.setVisibility(View.INVISIBLE);
			t88.setVisibility(View.INVISIBLE);
			t89.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<7){

			t71.setVisibility(View.INVISIBLE);
			t72.setVisibility(View.INVISIBLE);
			t73.setVisibility(View.INVISIBLE);
			t74.setVisibility(View.INVISIBLE);
			t75.setVisibility(View.INVISIBLE);
			t76.setVisibility(View.INVISIBLE);
			t77.setVisibility(View.INVISIBLE);
			t78.setVisibility(View.INVISIBLE);
			t79.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<6){

			t61.setVisibility(View.INVISIBLE);
			t62.setVisibility(View.INVISIBLE);
			t63.setVisibility(View.INVISIBLE);
			t64.setVisibility(View.INVISIBLE);
			t65.setVisibility(View.INVISIBLE);
			t66.setVisibility(View.INVISIBLE);
			t67.setVisibility(View.INVISIBLE);
			t68.setVisibility(View.INVISIBLE);
			t69.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<5){

			t51.setVisibility(View.INVISIBLE);
			t52.setVisibility(View.INVISIBLE);
			t53.setVisibility(View.INVISIBLE);
			t54.setVisibility(View.INVISIBLE);
			t55.setVisibility(View.INVISIBLE);
			t56.setVisibility(View.INVISIBLE);
			t57.setVisibility(View.INVISIBLE);
			t58.setVisibility(View.INVISIBLE);
			t59.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<4){

			t41.setVisibility(View.INVISIBLE);
			t42.setVisibility(View.INVISIBLE);
			t43.setVisibility(View.INVISIBLE);
			t44.setVisibility(View.INVISIBLE);
			t45.setVisibility(View.INVISIBLE);
			t46.setVisibility(View.INVISIBLE);
			t47.setVisibility(View.INVISIBLE);
			t48.setVisibility(View.INVISIBLE);
			t49.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<3){

			t31.setVisibility(View.INVISIBLE);
			t32.setVisibility(View.INVISIBLE);
			t33.setVisibility(View.INVISIBLE);
			t34.setVisibility(View.INVISIBLE);
			t35.setVisibility(View.INVISIBLE);
			t36.setVisibility(View.INVISIBLE);
			t37.setVisibility(View.INVISIBLE);
			t38.setVisibility(View.INVISIBLE);
			t39.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.m<2){

			t21.setVisibility(View.INVISIBLE);
			t22.setVisibility(View.INVISIBLE);
			t23.setVisibility(View.INVISIBLE);
			t24.setVisibility(View.INVISIBLE);
			t25.setVisibility(View.INVISIBLE);
			t26.setVisibility(View.INVISIBLE);
			t27.setVisibility(View.INVISIBLE);
			t28.setVisibility(View.INVISIBLE);
			t29.setVisibility(View.INVISIBLE);

		}

		if(MainActivity.n<9){

			t19.setVisibility(View.INVISIBLE);
			t29.setVisibility(View.INVISIBLE);
			t39.setVisibility(View.INVISIBLE);
			t49.setVisibility(View.INVISIBLE);
			t59.setVisibility(View.INVISIBLE);
			t69.setVisibility(View.INVISIBLE);
			t79.setVisibility(View.INVISIBLE);
			t89.setVisibility(View.INVISIBLE);
			t99.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<8){

			t18.setVisibility(View.INVISIBLE);
			t28.setVisibility(View.INVISIBLE);
			t38.setVisibility(View.INVISIBLE);
			t48.setVisibility(View.INVISIBLE);
			t58.setVisibility(View.INVISIBLE);
			t68.setVisibility(View.INVISIBLE);
			t78.setVisibility(View.INVISIBLE);
			t88.setVisibility(View.INVISIBLE);
			t98.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<7){

			t17.setVisibility(View.INVISIBLE);
			t27.setVisibility(View.INVISIBLE);
			t37.setVisibility(View.INVISIBLE);
			t47.setVisibility(View.INVISIBLE);
			t57.setVisibility(View.INVISIBLE);
			t67.setVisibility(View.INVISIBLE);
			t77.setVisibility(View.INVISIBLE);
			t87.setVisibility(View.INVISIBLE);
			t97.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<6){

			t16.setVisibility(View.INVISIBLE);
			t26.setVisibility(View.INVISIBLE);
			t36.setVisibility(View.INVISIBLE);
			t46.setVisibility(View.INVISIBLE);
			t56.setVisibility(View.INVISIBLE);
			t66.setVisibility(View.INVISIBLE);
			t76.setVisibility(View.INVISIBLE);
			t86.setVisibility(View.INVISIBLE);
			t96.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<5){

			t15.setVisibility(View.INVISIBLE);
			t25.setVisibility(View.INVISIBLE);
			t35.setVisibility(View.INVISIBLE);
			t45.setVisibility(View.INVISIBLE);
			t55.setVisibility(View.INVISIBLE);
			t65.setVisibility(View.INVISIBLE);
			t75.setVisibility(View.INVISIBLE);
			t85.setVisibility(View.INVISIBLE);
			t95.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<4){

			t14.setVisibility(View.INVISIBLE);
			t24.setVisibility(View.INVISIBLE);
			t34.setVisibility(View.INVISIBLE);
			t44.setVisibility(View.INVISIBLE);
			t54.setVisibility(View.INVISIBLE);
			t64.setVisibility(View.INVISIBLE);
			t74.setVisibility(View.INVISIBLE);
			t84.setVisibility(View.INVISIBLE);
			t94.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<3){

			t13.setVisibility(View.INVISIBLE);
			t23.setVisibility(View.INVISIBLE);
			t33.setVisibility(View.INVISIBLE);
			t43.setVisibility(View.INVISIBLE);
			t53.setVisibility(View.INVISIBLE);
			t63.setVisibility(View.INVISIBLE);
			t73.setVisibility(View.INVISIBLE);
			t83.setVisibility(View.INVISIBLE);
			t93.setVisibility(View.INVISIBLE);

		}
		if(MainActivity.n<2){

			t12.setVisibility(View.INVISIBLE);
			t22.setVisibility(View.INVISIBLE);
			t32.setVisibility(View.INVISIBLE);
			t42.setVisibility(View.INVISIBLE);
			t52.setVisibility(View.INVISIBLE);
			t62.setVisibility(View.INVISIBLE);
			t72.setVisibility(View.INVISIBLE);
			t82.setVisibility(View.INVISIBLE);
			t92.setVisibility(View.INVISIBLE);

		}


		Button b1 = (Button)findViewById(R.id.button1);
		b1.setOnClickListener(this);


	}


	public void onClick(View v) {
		// TODO Auto-generated method stub

		EditText t11=(EditText)findViewById(R.id.editText1);
		EditText t21=(EditText)findViewById(R.id.EditText01);
		EditText t31=(EditText)findViewById(R.id.EditText03);
		EditText t41=(EditText)findViewById(R.id.EditText04);
		EditText t51=(EditText)findViewById(R.id.EditText05);
		EditText t61=(EditText)findViewById(R.id.EditText06);
		EditText t71=(EditText)findViewById(R.id.EditText07);
		EditText t81=(EditText)findViewById(R.id.EditText08);
		EditText t91=(EditText)findViewById(R.id.editText68);

		EditText t12=(EditText)findViewById(R.id.editText2);
		EditText t22=(EditText)findViewById(R.id.editText3);
		EditText t32=(EditText)findViewById(R.id.editText4);
		EditText t42=(EditText)findViewById(R.id.editText5);
		EditText t52=(EditText)findViewById(R.id.editText7);
		EditText t62=(EditText)findViewById(R.id.editText8);
		EditText t72=(EditText)findViewById(R.id.editText6);
		EditText t82=(EditText)findViewById(R.id.editText9);
		EditText t92=(EditText)findViewById(R.id.editText69);

		EditText t13=(EditText)findViewById(R.id.editText10);
		EditText t23=(EditText)findViewById(R.id.editText11);
		EditText t33=(EditText)findViewById(R.id.editText12);
		EditText t43=(EditText)findViewById(R.id.editText14);
		EditText t53=(EditText)findViewById(R.id.editText15);
		EditText t63=(EditText)findViewById(R.id.editText13);
		EditText t73=(EditText)findViewById(R.id.editText16);
		EditText t83=(EditText)findViewById(R.id.editText17);
		EditText t93=(EditText)findViewById(R.id.editText70);

		EditText t14=(EditText)findViewById(R.id.editText18);
		EditText t24=(EditText)findViewById(R.id.editText19);
		EditText t34=(EditText)findViewById(R.id.editText21);
		EditText t44=(EditText)findViewById(R.id.editText22);
		EditText t54=(EditText)findViewById(R.id.editText20);
		EditText t64=(EditText)findViewById(R.id.editText23);
		EditText t74=(EditText)findViewById(R.id.editText24);
		EditText t84=(EditText)findViewById(R.id.editText25);
		EditText t94=(EditText)findViewById(R.id.editText71);

		EditText t15=(EditText)findViewById(R.id.editText27);
		EditText t25=(EditText)findViewById(R.id.editText28);
		EditText t35=(EditText)findViewById(R.id.editText29);
		EditText t45=(EditText)findViewById(R.id.editText31);
		EditText t55=(EditText)findViewById(R.id.editText32);
		EditText t65=(EditText)findViewById(R.id.editText33);
		EditText t75=(EditText)findViewById(R.id.editText34);
		EditText t85=(EditText)findViewById(R.id.editText35);
		EditText t95=(EditText)findViewById(R.id.editText72);

		EditText t16=(EditText)findViewById(R.id.editText36);
		EditText t26=(EditText)findViewById(R.id.editText37);
		EditText t36=(EditText)findViewById(R.id.editText38);
		EditText t46=(EditText)findViewById(R.id.editText39);
		EditText t56=(EditText)findViewById(R.id.editText40);
		EditText t66=(EditText)findViewById(R.id.editText41);
		EditText t76=(EditText)findViewById(R.id.editText42);
		EditText t86=(EditText)findViewById(R.id.editText43);
		EditText t96=(EditText)findViewById(R.id.editText73);

		EditText t17=(EditText)findViewById(R.id.editText44);
		EditText t27=(EditText)findViewById(R.id.editText45);
		EditText t37=(EditText)findViewById(R.id.editText46);
		EditText t47=(EditText)findViewById(R.id.editText47);
		EditText t57=(EditText)findViewById(R.id.editText48);
		EditText t67=(EditText)findViewById(R.id.editText49);
		EditText t77=(EditText)findViewById(R.id.editText50);
		EditText t87=(EditText)findViewById(R.id.editText51);
		EditText t97=(EditText)findViewById(R.id.editText74);

		EditText t18=(EditText)findViewById(R.id.editText52);
		EditText t28=(EditText)findViewById(R.id.editText53);
		EditText t38=(EditText)findViewById(R.id.editText54);
		EditText t48=(EditText)findViewById(R.id.editText55);
		EditText t58=(EditText)findViewById(R.id.editText56);
		EditText t68=(EditText)findViewById(R.id.editText57);
		EditText t78=(EditText)findViewById(R.id.editText58);
		EditText t88=(EditText)findViewById(R.id.editText59);
		EditText t98=(EditText)findViewById(R.id.editText75);

		EditText t19=(EditText)findViewById(R.id.editText60);
		EditText t29=(EditText)findViewById(R.id.editText61);
		EditText t39=(EditText)findViewById(R.id.editText62);
		EditText t49=(EditText)findViewById(R.id.editText63);
		EditText t59=(EditText)findViewById(R.id.editText64);
		EditText t69=(EditText)findViewById(R.id.editText65);
		EditText t79=(EditText)findViewById(R.id.editText66);
		EditText t89=(EditText)findViewById(R.id.editText67);
		EditText t99=(EditText)findViewById(R.id.editText76);

		matrix = new float[9][9];
		matrizea1313(9, 9);


		if(v.getId()==findViewById(R.id.button1).getId()){

			String s11 = t11.getText().toString();
			if(s11.compareTo("")==0){


				t11.setBackgroundColor(Color.RED);
			}
			else{

				matrix[0][0] = Float.parseFloat(s11);
				t11.setBackgroundColor(Color.TRANSPARENT);

			}

			String s12 = t12.getText().toString();
			if(s12.compareTo("")==0){


				t12.setBackgroundColor(Color.RED);
			}
			else{

				matrix[0][1] = Float.parseFloat(s12);
				t12.setBackgroundColor(Color.TRANSPARENT);


			}

			String s13 = t13.getText().toString();
			if(s13.compareTo("")==0){


				t13.setBackgroundColor(Color.RED);
			}
			else{

				matrix[0][2] = Float.parseFloat(s13);
				t13.setBackgroundColor(Color.TRANSPARENT);


			}

			String s14 = t14.getText().toString();
			if(s14.compareTo("")==0){


				t14.setBackgroundColor(Color.RED);
			}
			else{

				matrix[0][3] = Float.parseFloat(s14);
				t14.setBackgroundColor(Color.TRANSPARENT);


			}
			String s15 = t15.getText().toString();
			if(s15.compareTo("")==0){


				t15.setBackgroundColor(Color.RED);
			}
			else{

				matrix[0][4] = Float.parseFloat(s15);
				t15.setBackgroundColor(Color.TRANSPARENT);


			}
			String s16 = t16.getText().toString();
			if(s16.compareTo("")==0){


				t16.setBackgroundColor(Color.RED);
			}
			else{

				matrix[0][5] = Float.parseFloat(s16);
				t16.setBackgroundColor(Color.TRANSPARENT);


			}
			String s17 = t17.getText().toString();
			if(s17.compareTo("")==0){


				t17.setBackgroundColor(Color.RED);
			}
			else{

				matrix[0][6] = Float.parseFloat(s17);
				t17.setBackgroundColor(Color.TRANSPARENT);


			}
			String s18 = t18.getText().toString();
			if(s18.compareTo("")==0){


				t18.setBackgroundColor(Color.RED);
			}
			else{

				matrix[0][7] = Float.parseFloat(s18);
				t18.setBackgroundColor(Color.TRANSPARENT);


			}
			String s19 = t19.getText().toString();
			if(s19.compareTo("")==0){


				t19.setBackgroundColor(Color.RED);
			}
			else{

				matrix[0][8] = Float.parseFloat(s19);
				t19.setBackgroundColor(Color.TRANSPARENT);


			}

			String s21 = t21.getText().toString();
			if(s21.compareTo("")==0){


				t21.setBackgroundColor(Color.RED);
			}
			else{

				matrix[1][0] = Float.parseFloat(s21);
				t21.setBackgroundColor(Color.TRANSPARENT);


			}

			String s22 = t22.getText().toString();
			if(s22.compareTo("")==0){


				t22.setBackgroundColor(Color.RED);
			}
			else{

				matrix[1][1] = Float.parseFloat(s22);
				t22.setBackgroundColor(Color.TRANSPARENT);


			}

			String s23 = t23.getText().toString();
			if(s23.compareTo("")==0){


				t23.setBackgroundColor(Color.RED);
			}
			else{

				matrix[1][2] = Float.parseFloat(s23);
				t23.setBackgroundColor(Color.TRANSPARENT);


			}

			String s24 = t24.getText().toString();
			if(s24.compareTo("")==0){


				t24.setBackgroundColor(Color.RED);
			}
			else{

				matrix[1][3] = Float.parseFloat(s24);
				t24.setBackgroundColor(Color.TRANSPARENT);


			}
			String s25 = t25.getText().toString();
			if(s25.compareTo("")==0){


				t25.setBackgroundColor(Color.RED);
			}
			else{

				matrix[1][4] = Float.parseFloat(s25);
				t25.setBackgroundColor(Color.TRANSPARENT);


			}
			String s26 = t26.getText().toString();
			if(s26.compareTo("")==0){


				t26.setBackgroundColor(Color.RED);
			}
			else{

				matrix[1][5] = Float.parseFloat(s26);
				t26.setBackgroundColor(Color.TRANSPARENT);


			}
			String s27 = t27.getText().toString();
			if(s27.compareTo("")==0){


				t27.setBackgroundColor(Color.RED);
			}
			else{

				matrix[1][6] = Float.parseFloat(s27);
				t27.setBackgroundColor(Color.TRANSPARENT);


			}
			String s28 = t28.getText().toString();
			if(s28.compareTo("")==0){


				t28.setBackgroundColor(Color.RED);
			}
			else{

				matrix[1][7] = Float.parseFloat(s28);
				t28.setBackgroundColor(Color.TRANSPARENT);


			}
			String s29 = t29.getText().toString();
			if(s29.compareTo("")==0){


				t29.setBackgroundColor(Color.RED);
			}
			else{

				matrix[1][8] = Float.parseFloat(s29);
				t29.setBackgroundColor(Color.TRANSPARENT);


			}



			String s31 = t31.getText().toString();
			if(s31.compareTo("")==0){


				t31.setBackgroundColor(Color.RED);
			}
			else{

				matrix[2][0] = Float.parseFloat(s31);
				t31.setBackgroundColor(Color.TRANSPARENT);


			}

			String s32 = t32.getText().toString();
			if(s32.compareTo("")==0){


				t32.setBackgroundColor(Color.RED);
			}
			else{

				matrix[2][1] = Float.parseFloat(s32);
				t32.setBackgroundColor(Color.TRANSPARENT);


			}

			String s33 = t33.getText().toString();
			if(s33.compareTo("")==0){


				t33.setBackgroundColor(Color.RED);
			}
			else{

				matrix[2][2] = Float.parseFloat(s33);
				t33.setBackgroundColor(Color.TRANSPARENT);


			}

			String s34 = t34.getText().toString();
			if(s34.compareTo("")==0){


				t34.setBackgroundColor(Color.RED);
			}
			else{

				matrix[2][3] = Float.parseFloat(s34);
				t34.setBackgroundColor(Color.TRANSPARENT);


			}
			String s35 = t35.getText().toString();
			if(s35.compareTo("")==0){


				t35.setBackgroundColor(Color.RED);
			}
			else{

				matrix[2][4] = Float.parseFloat(s35);
				t35.setBackgroundColor(Color.TRANSPARENT);


			}
			String s36 = t36.getText().toString();
			if(s36.compareTo("")==0){


				t36.setBackgroundColor(Color.RED);
			}
			else{

				matrix[2][5] = Float.parseFloat(s36);
				t36.setBackgroundColor(Color.TRANSPARENT);


			}
			String s37 = t37.getText().toString();
			if(s37.compareTo("")==0){


				t37.setBackgroundColor(Color.RED);
			}
			else{

				matrix[2][6] = Float.parseFloat(s37);
				t37.setBackgroundColor(Color.TRANSPARENT);


			}
			String s38 = t38.getText().toString();
			if(s38.compareTo("")==0){


				t38.setBackgroundColor(Color.RED);
			}
			else{

				matrix[2][7] = Float.parseFloat(s38);
				t38.setBackgroundColor(Color.TRANSPARENT);


			}
			String s39 = t39.getText().toString();
			if(s39.compareTo("")==0){


				t39.setBackgroundColor(Color.RED);
			}
			else{

				matrix[2][8] = Float.parseFloat(s39);
				t39.setBackgroundColor(Color.TRANSPARENT);


			}

			String s41 = t41.getText().toString();
			if(s41.compareTo("")==0){


				t41.setBackgroundColor(Color.RED);
			}
			else{

				matrix[3][0] = Float.parseFloat(s41);
				t41.setBackgroundColor(Color.TRANSPARENT);


			}

			String s42 = t42.getText().toString();
			if(s42.compareTo("")==0){


				t42.setBackgroundColor(Color.RED);
			}
			else{

				matrix[3][1] = Float.parseFloat(s42);
				t42.setBackgroundColor(Color.TRANSPARENT);


			}

			String s43 = t43.getText().toString();
			if(s43.compareTo("")==0){


				t43.setBackgroundColor(Color.RED);
			}
			else{

				matrix[3][2] = Float.parseFloat(s43);
				t43.setBackgroundColor(Color.TRANSPARENT);


			}

			String s44 = t44.getText().toString();
			if(s44.compareTo("")==0){


				t44.setBackgroundColor(Color.RED);
			}
			else{

				matrix[3][3] = Float.parseFloat(s44);
				t44.setBackgroundColor(Color.TRANSPARENT);


			}
			String s45 = t45.getText().toString();
			if(s45.compareTo("")==0){


				t45.setBackgroundColor(Color.RED);
			}
			else{

				matrix[3][4] = Float.parseFloat(s45);
				t45.setBackgroundColor(Color.TRANSPARENT);


			}
			String s46 = t46.getText().toString();
			if(s46.compareTo("")==0){


				t46.setBackgroundColor(Color.RED);
			}
			else{

				matrix[3][5] = Float.parseFloat(s46);
				t46.setBackgroundColor(Color.TRANSPARENT);


			}
			String s47 = t47.getText().toString();
			if(s47.compareTo("")==0){


				t47.setBackgroundColor(Color.RED);
			}
			else{

				matrix[3][6] = Float.parseFloat(s47);
				t47.setBackgroundColor(Color.TRANSPARENT);


			}
			String s48 = t48.getText().toString();
			if(s48.compareTo("")==0){


				t48.setBackgroundColor(Color.RED);
			}
			else{

				matrix[3][7] = Float.parseFloat(s48);
				t48.setBackgroundColor(Color.TRANSPARENT);


			}
			String s49 = t49.getText().toString();
			if(s49.compareTo("")==0){


				t49.setBackgroundColor(Color.RED);
			}
			else{

				matrix[3][8] = Float.parseFloat(s49);
				t49.setBackgroundColor(Color.TRANSPARENT);


			}

			String s51 = t51.getText().toString();
			if(s51.compareTo("")==0){


				t51.setBackgroundColor(Color.RED);
			}
			else{

				matrix[4][0] = Float.parseFloat(s51);
				t51.setBackgroundColor(Color.TRANSPARENT);


			}

			String s52 = t52.getText().toString();
			if(s52.compareTo("")==0){


				t52.setBackgroundColor(Color.RED);
			}
			else{

				matrix[4][1] = Float.parseFloat(s52);
				t52.setBackgroundColor(Color.TRANSPARENT);


			}

			String s53 = t53.getText().toString();
			if(s53.compareTo("")==0){


				t53.setBackgroundColor(Color.RED);
			}
			else{

				matrix[4][2] = Float.parseFloat(s53);
				t53.setBackgroundColor(Color.TRANSPARENT);


			}

			String s54 = t54.getText().toString();
			if(s54.compareTo("")==0){


				t54.setBackgroundColor(Color.RED);
			}
			else{

				matrix[4][3] = Float.parseFloat(s54);
				t54.setBackgroundColor(Color.TRANSPARENT);


			}
			String s55 = t55.getText().toString();
			if(s55.compareTo("")==0){


				t55.setBackgroundColor(Color.RED);
			}
			else{

				matrix[4][4] = Float.parseFloat(s55);
				t55.setBackgroundColor(Color.TRANSPARENT);


			}
			String s56 = t56.getText().toString();
			if(s56.compareTo("")==0){


				t56.setBackgroundColor(Color.RED);
			}
			else{

				matrix[4][5] = Float.parseFloat(s56);
				t56.setBackgroundColor(Color.TRANSPARENT);


			}
			String s57 = t57.getText().toString();
			if(s57.compareTo("")==0){


				t57.setBackgroundColor(Color.RED);

			}
			else{

				matrix[4][6] = Float.parseFloat(s57);
				t57.setBackgroundColor(Color.TRANSPARENT);


			}
			String s58 = t58.getText().toString();
			if(s58.compareTo("")==0){


				t58.setBackgroundColor(Color.RED);
			}
			else{

				matrix[4][7] = Float.parseFloat(s58);
				t58.setBackgroundColor(Color.TRANSPARENT);


			}
			String s59 = t59.getText().toString();
			if(s59.compareTo("")==0){


				t59.setBackgroundColor(Color.RED);
			}
			else{

				matrix[4][8] = Float.parseFloat(s59);
				t59.setBackgroundColor(Color.TRANSPARENT);


			}


			String s61 = t61.getText().toString();
			if(s61.compareTo("")==0){


				t61.setBackgroundColor(Color.RED);
			}
			else{

				matrix[5][0] = Float.parseFloat(s61);
				t61.setBackgroundColor(Color.TRANSPARENT);


			}

			String s62 = t62.getText().toString();
			if(s62.compareTo("")==0){


				t62.setBackgroundColor(Color.RED);
			}
			else{

				matrix[5][1] = Float.parseFloat(s62);
				t62.setBackgroundColor(Color.TRANSPARENT);


			}

			String s63 = t63.getText().toString();
			if(s63.compareTo("")==0){


				t63.setBackgroundColor(Color.RED);
			}
			else{

				matrix[5][2] = Float.parseFloat(s63);
				t63.setBackgroundColor(Color.TRANSPARENT);


			}

			String s64 = t64.getText().toString();
			if(s64.compareTo("")==0){


				t64.setBackgroundColor(Color.RED);
			}
			else{

				matrix[5][3] = Float.parseFloat(s64);
				t64.setBackgroundColor(Color.TRANSPARENT);


			}
			String s65 = t65.getText().toString();
			if(s65.compareTo("")==0){


				t65.setBackgroundColor(Color.RED);
			}
			else{

				matrix[5][4] = Float.parseFloat(s65);
				t65.setBackgroundColor(Color.TRANSPARENT);


			}
			String s66 = t66.getText().toString();
			if(s66.compareTo("")==0){


				t66.setBackgroundColor(Color.RED);

			}
			else{

				matrix[5][5] = Float.parseFloat(s66);
				t66.setBackgroundColor(Color.TRANSPARENT);


			}
			String s67 = t67.getText().toString();
			if(s67.compareTo("")==0){


				t67.setBackgroundColor(Color.RED);
			}
			else{

				matrix[5][6] = Float.parseFloat(s67);
				t67.setBackgroundColor(Color.TRANSPARENT);


			}
			String s68 = t68.getText().toString();
			if(s68.compareTo("")==0){


				t68.setBackgroundColor(Color.RED);
			}
			else{

				matrix[5][7] = Float.parseFloat(s68);
				t68.setBackgroundColor(Color.TRANSPARENT);


			}
			String s69 = t69.getText().toString();
			if(s69.compareTo("")==0){


				t69.setBackgroundColor(Color.RED);
			}
			else{

				matrix[5][8] = Float.parseFloat(s69);
				t69.setBackgroundColor(Color.TRANSPARENT);


			}

			String s71 = t71.getText().toString();
			if(s71.compareTo("")==0){


				t71.setBackgroundColor(Color.RED);
			}
			else{

				matrix[6][0] = Float.parseFloat(s71);
				t71.setBackgroundColor(Color.TRANSPARENT);


			}

			String s72 = t72.getText().toString();
			if(s72.compareTo("")==0){


				t72.setBackgroundColor(Color.RED);
			}
			else{

				matrix[6][1] = Float.parseFloat(s72);
				t72.setBackgroundColor(Color.TRANSPARENT);


			}

			String s73 = t73.getText().toString();
			if(s73.compareTo("")==0){


				t73.setBackgroundColor(Color.RED);
			}
			else{

				matrix[6][2] = Float.parseFloat(s73);
				t73.setBackgroundColor(Color.TRANSPARENT);


			}

			String s74 = t74.getText().toString();
			if(s74.compareTo("")==0){


				t74.setBackgroundColor(Color.RED);
			}
			else{

				matrix[6][3] = Float.parseFloat(s74);
				t74.setBackgroundColor(Color.TRANSPARENT);


			}
			String s75 = t75.getText().toString();
			if(s75.compareTo("")==0){


				t75.setBackgroundColor(Color.RED);
			}
			else{

				matrix[6][4] = Float.parseFloat(s75);
				t75.setBackgroundColor(Color.TRANSPARENT);


			}
			String s76 = t76.getText().toString();
			if(s76.compareTo("")==0){


				t76.setBackgroundColor(Color.RED);
			}
			else{

				matrix[6][5] = Float.parseFloat(s76);
				t76.setBackgroundColor(Color.TRANSPARENT);


			}
			String s77 = t77.getText().toString();
			if(s77.compareTo("")==0){


				t77.setBackgroundColor(Color.RED);
			}
			else{

				matrix[6][6] = Float.parseFloat(s77);
				t77.setBackgroundColor(Color.TRANSPARENT);


			}
			String s78 = t78.getText().toString();
			if(s78.compareTo("")==0){


				t78.setBackgroundColor(Color.RED);
			}
			else{

				matrix[6][7] = Float.parseFloat(s78);
				t78.setBackgroundColor(Color.TRANSPARENT);


			}
			String s79 = t79.getText().toString();
			if(s79.compareTo("")==0){


				t79.setBackgroundColor(Color.RED);
			}
			else{

				matrix[6][8] = Float.parseFloat(s79);
				t79.setBackgroundColor(Color.TRANSPARENT);


			}


			String s81 = t81.getText().toString();
			if(s81.compareTo("")==0){


				t81.setBackgroundColor(Color.RED);
			}
			else{

				matrix[7][0] = Float.parseFloat(s81);
				t81.setBackgroundColor(Color.TRANSPARENT);


			}

			String s82 = t82.getText().toString();
			if(s82.compareTo("")==0){


				t82.setBackgroundColor(Color.RED);
			}
			else{

				matrix[7][1] = Float.parseFloat(s82);
				t82.setBackgroundColor(Color.TRANSPARENT);


			}

			String s83 = t83.getText().toString();
			if(s83.compareTo("")==0){


				t83.setBackgroundColor(Color.RED);
			}
			else{

				matrix[7][2] = Float.parseFloat(s83);
				t83.setBackgroundColor(Color.TRANSPARENT);


			}

			String s84 = t84.getText().toString();
			if(s84.compareTo("")==0){


				t84.setBackgroundColor(Color.RED);
			}
			else{

				matrix[7][3] = Float.parseFloat(s84);
				t84.setBackgroundColor(Color.TRANSPARENT);


			}
			String s85 = t85.getText().toString();
			if(s85.compareTo("")==0){


				t85.setBackgroundColor(Color.RED);
			}
			else{

				matrix[7][4] = Float.parseFloat(s85);
				t85.setBackgroundColor(Color.TRANSPARENT);


			}
			String s86 = t86.getText().toString();
			if(s86.compareTo("")==0){


				t86.setBackgroundColor(Color.RED);
			}
			else{

				matrix[7][5] = Float.parseFloat(s86);
				t86.setBackgroundColor(Color.TRANSPARENT);


			}
			String s87 = t87.getText().toString();
			if(s87.compareTo("")==0){


				t87.setBackgroundColor(Color.RED);
			}
			else{

				matrix[7][6] = Float.parseFloat(s87);
				t87.setBackgroundColor(Color.TRANSPARENT);


			}
			String s88 = t88.getText().toString();
			if(s88.compareTo("")==0){


				t88.setBackgroundColor(Color.RED);
			}
			else{

				matrix[7][7] = Float.parseFloat(s88);
				t88.setBackgroundColor(Color.TRANSPARENT);


			}
			String s89 = t89.getText().toString();
			if(s89.compareTo("")==0){


				t89.setBackgroundColor(Color.RED);
			}
			else{

				matrix[7][8] = Float.parseFloat(s89);
				t89.setBackgroundColor(Color.TRANSPARENT);


			}

			String s91 = t91.getText().toString();
			if(s91.compareTo("")==0){


				t91.setBackgroundColor(Color.RED);
			}
			else{

				matrix[8][0] = Float.parseFloat(s91);
				t91.setBackgroundColor(Color.TRANSPARENT);


			}

			String s92 = t92.getText().toString();
			if(s92.compareTo("")==0){


				t92.setBackgroundColor(Color.RED);
			}
			else{

				matrix[8][1] = Float.parseFloat(s92);
				t92.setBackgroundColor(Color.TRANSPARENT);


			}

			String s93 = t93.getText().toString();
			if(s93.compareTo("")==0){


				t93.setBackgroundColor(Color.RED);
			}
			else{

				matrix[8][2] = Float.parseFloat(s93);
				t93.setBackgroundColor(Color.TRANSPARENT);


			}

			String s94 = t94.getText().toString();
			if(s94.compareTo("")==0){


				t94.setBackgroundColor(Color.RED);
			}
			else{

				matrix[8][3] = Float.parseFloat(s94);
				t94.setBackgroundColor(Color.TRANSPARENT);


			}
			String s95 = t95.getText().toString();
			if(s95.compareTo("")==0){


				t95.setBackgroundColor(Color.RED);
			}
			else{

				matrix[8][4] = Float.parseFloat(s95);
				t95.setBackgroundColor(Color.TRANSPARENT);


			}
			String s96 = t96.getText().toString();
			if(s96.compareTo("")==0){


				t96.setBackgroundColor(Color.RED);
			}
			else{

				matrix[8][5] = Float.parseFloat(s96);
				t96.setBackgroundColor(Color.TRANSPARENT);


			}
			String s97 = t97.getText().toString();
			if(s97.compareTo("")==0){


				t97.setBackgroundColor(Color.RED);
			}
			else{

				matrix[8][6] = Float.parseFloat(s97);
				t97.setBackgroundColor(Color.TRANSPARENT);


			}
			String s98 = t98.getText().toString();
			if(s98.compareTo("")==0){


				t98.setBackgroundColor(Color.RED);
			}
			else{

				matrix[8][7] = Float.parseFloat(s98);
				t98.setBackgroundColor(Color.TRANSPARENT);


			}
			String s99 = t99.getText().toString();
			if(s99.compareTo("")==0){


				t99.setBackgroundColor(Color.RED);
			}
			else{

				matrix[8][8] = Float.parseFloat(s99);
				t99.setBackgroundColor(Color.TRANSPARENT);


			}
			if(matrizeaEgiaztatu(MainActivity.m, MainActivity.n)){

				metodoak1.matrizeaInprimatu(matrix, MainActivity.m, MainActivity.n);		

				Intent hurren= new Intent(this, MainActivity3.class);
				startActivity(hurren);
			}

		}



	}

	private boolean matrizeaEgiaztatu(int m, int n){

		for(int i=0; i<m; i++)
			for(int j=0; j<n; j++)
				if(matrix[i][j]==-131313)return false;
		return true;


	}

	private void matrizea1313(int m, int n){

		for(int i=0; i<m; i++)
		{
			for(int j=0; j<n; j++)
				matrix[i][j]=-131313;
		}

	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_activity2, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {

			EditText t11=(EditText)findViewById(R.id.editText1);
			EditText t21=(EditText)findViewById(R.id.EditText01);
			EditText t31=(EditText)findViewById(R.id.EditText03);
			EditText t41=(EditText)findViewById(R.id.EditText04);
			EditText t51=(EditText)findViewById(R.id.EditText05);
			EditText t61=(EditText)findViewById(R.id.EditText06);
			EditText t71=(EditText)findViewById(R.id.EditText07);
			EditText t81=(EditText)findViewById(R.id.EditText08);
			EditText t91=(EditText)findViewById(R.id.editText68);

			EditText t12=(EditText)findViewById(R.id.editText2);
			EditText t22=(EditText)findViewById(R.id.editText3);
			EditText t32=(EditText)findViewById(R.id.editText4);
			EditText t42=(EditText)findViewById(R.id.editText5);
			EditText t52=(EditText)findViewById(R.id.editText7);
			EditText t62=(EditText)findViewById(R.id.editText8);
			EditText t72=(EditText)findViewById(R.id.editText6);
			EditText t82=(EditText)findViewById(R.id.editText9);
			EditText t92=(EditText)findViewById(R.id.editText69);

			EditText t13=(EditText)findViewById(R.id.editText10);
			EditText t23=(EditText)findViewById(R.id.editText11);
			EditText t33=(EditText)findViewById(R.id.editText12);
			EditText t43=(EditText)findViewById(R.id.editText14);
			EditText t53=(EditText)findViewById(R.id.editText15);
			EditText t63=(EditText)findViewById(R.id.editText13);
			EditText t73=(EditText)findViewById(R.id.editText16);
			EditText t83=(EditText)findViewById(R.id.editText17);
			EditText t93=(EditText)findViewById(R.id.editText70);

			EditText t14=(EditText)findViewById(R.id.editText18);
			EditText t24=(EditText)findViewById(R.id.editText19);
			EditText t34=(EditText)findViewById(R.id.editText21);
			EditText t44=(EditText)findViewById(R.id.editText22);
			EditText t54=(EditText)findViewById(R.id.editText20);
			EditText t64=(EditText)findViewById(R.id.editText23);
			EditText t74=(EditText)findViewById(R.id.editText24);
			EditText t84=(EditText)findViewById(R.id.editText25);
			EditText t94=(EditText)findViewById(R.id.editText71);

			EditText t15=(EditText)findViewById(R.id.editText27);
			EditText t25=(EditText)findViewById(R.id.editText28);
			EditText t35=(EditText)findViewById(R.id.editText29);
			EditText t45=(EditText)findViewById(R.id.editText31);
			EditText t55=(EditText)findViewById(R.id.editText32);
			EditText t65=(EditText)findViewById(R.id.editText33);
			EditText t75=(EditText)findViewById(R.id.editText34);
			EditText t85=(EditText)findViewById(R.id.editText35);
			EditText t95=(EditText)findViewById(R.id.editText72);

			EditText t16=(EditText)findViewById(R.id.editText36);
			EditText t26=(EditText)findViewById(R.id.editText37);
			EditText t36=(EditText)findViewById(R.id.editText38);
			EditText t46=(EditText)findViewById(R.id.editText39);
			EditText t56=(EditText)findViewById(R.id.editText40);
			EditText t66=(EditText)findViewById(R.id.editText41);
			EditText t76=(EditText)findViewById(R.id.editText42);
			EditText t86=(EditText)findViewById(R.id.editText43);
			EditText t96=(EditText)findViewById(R.id.editText73);

			EditText t17=(EditText)findViewById(R.id.editText44);
			EditText t27=(EditText)findViewById(R.id.editText45);
			EditText t37=(EditText)findViewById(R.id.editText46);
			EditText t47=(EditText)findViewById(R.id.editText47);
			EditText t57=(EditText)findViewById(R.id.editText48);
			EditText t67=(EditText)findViewById(R.id.editText49);
			EditText t77=(EditText)findViewById(R.id.editText50);
			EditText t87=(EditText)findViewById(R.id.editText51);
			EditText t97=(EditText)findViewById(R.id.editText74);

			EditText t18=(EditText)findViewById(R.id.editText52);
			EditText t28=(EditText)findViewById(R.id.editText53);
			EditText t38=(EditText)findViewById(R.id.editText54);
			EditText t48=(EditText)findViewById(R.id.editText55);
			EditText t58=(EditText)findViewById(R.id.editText56);
			EditText t68=(EditText)findViewById(R.id.editText57);
			EditText t78=(EditText)findViewById(R.id.editText58);
			EditText t88=(EditText)findViewById(R.id.editText59);
			EditText t98=(EditText)findViewById(R.id.editText75);

			EditText t19=(EditText)findViewById(R.id.editText60);
			EditText t29=(EditText)findViewById(R.id.editText61);
			EditText t39=(EditText)findViewById(R.id.editText62);
			EditText t49=(EditText)findViewById(R.id.editText63);
			EditText t59=(EditText)findViewById(R.id.editText64);
			EditText t69=(EditText)findViewById(R.id.editText65);
			EditText t79=(EditText)findViewById(R.id.editText66);
			EditText t89=(EditText)findViewById(R.id.editText67);
			EditText t99=(EditText)findViewById(R.id.editText76);

			matrix = new float[9][9];
			matrizea1313(9, 9);



				String s11 = t11.getText().toString();
				if(s11.compareTo("")==0){


					t11.setBackgroundColor(Color.RED);
				}
				else{

					matrix[0][0] = Float.parseFloat(s11);
					t11.setBackgroundColor(Color.TRANSPARENT);

				}

				String s12 = t12.getText().toString();
				if(s12.compareTo("")==0){


					t12.setBackgroundColor(Color.RED);
				}
				else{

					matrix[0][1] = Float.parseFloat(s12);
					t12.setBackgroundColor(Color.TRANSPARENT);


				}

				String s13 = t13.getText().toString();
				if(s13.compareTo("")==0){


					t13.setBackgroundColor(Color.RED);
				}
				else{

					matrix[0][2] = Float.parseFloat(s13);
					t13.setBackgroundColor(Color.TRANSPARENT);


				}

				String s14 = t14.getText().toString();
				if(s14.compareTo("")==0){


					t14.setBackgroundColor(Color.RED);
				}
				else{

					matrix[0][3] = Float.parseFloat(s14);
					t14.setBackgroundColor(Color.TRANSPARENT);


				}
				String s15 = t15.getText().toString();
				if(s15.compareTo("")==0){


					t15.setBackgroundColor(Color.RED);
				}
				else{

					matrix[0][4] = Float.parseFloat(s15);
					t15.setBackgroundColor(Color.TRANSPARENT);


				}
				String s16 = t16.getText().toString();
				if(s16.compareTo("")==0){


					t16.setBackgroundColor(Color.RED);
				}
				else{

					matrix[0][5] = Float.parseFloat(s16);
					t16.setBackgroundColor(Color.TRANSPARENT);


				}
				String s17 = t17.getText().toString();
				if(s17.compareTo("")==0){


					t17.setBackgroundColor(Color.RED);
				}
				else{

					matrix[0][6] = Float.parseFloat(s17);
					t17.setBackgroundColor(Color.TRANSPARENT);


				}
				String s18 = t18.getText().toString();
				if(s18.compareTo("")==0){


					t18.setBackgroundColor(Color.RED);
				}
				else{

					matrix[0][7] = Float.parseFloat(s18);
					t18.setBackgroundColor(Color.TRANSPARENT);


				}
				String s19 = t19.getText().toString();
				if(s19.compareTo("")==0){


					t19.setBackgroundColor(Color.RED);
				}
				else{

					matrix[0][8] = Float.parseFloat(s19);
					t19.setBackgroundColor(Color.TRANSPARENT);


				}

				String s21 = t21.getText().toString();
				if(s21.compareTo("")==0){


					t21.setBackgroundColor(Color.RED);
				}
				else{

					matrix[1][0] = Float.parseFloat(s21);
					t21.setBackgroundColor(Color.TRANSPARENT);


				}

				String s22 = t22.getText().toString();
				if(s22.compareTo("")==0){


					t22.setBackgroundColor(Color.RED);
				}
				else{

					matrix[1][1] = Float.parseFloat(s22);
					t22.setBackgroundColor(Color.TRANSPARENT);


				}

				String s23 = t23.getText().toString();
				if(s23.compareTo("")==0){


					t23.setBackgroundColor(Color.RED);
				}
				else{

					matrix[1][2] = Float.parseFloat(s23);
					t23.setBackgroundColor(Color.TRANSPARENT);


				}

				String s24 = t24.getText().toString();
				if(s24.compareTo("")==0){


					t24.setBackgroundColor(Color.RED);
				}
				else{

					matrix[1][3] = Float.parseFloat(s24);
					t24.setBackgroundColor(Color.TRANSPARENT);


				}
				String s25 = t25.getText().toString();
				if(s25.compareTo("")==0){


					t25.setBackgroundColor(Color.RED);
				}
				else{

					matrix[1][4] = Float.parseFloat(s25);
					t25.setBackgroundColor(Color.TRANSPARENT);


				}
				String s26 = t26.getText().toString();
				if(s26.compareTo("")==0){


					t26.setBackgroundColor(Color.RED);
				}
				else{

					matrix[1][5] = Float.parseFloat(s26);
					t26.setBackgroundColor(Color.TRANSPARENT);


				}
				String s27 = t27.getText().toString();
				if(s27.compareTo("")==0){


					t27.setBackgroundColor(Color.RED);
				}
				else{

					matrix[1][6] = Float.parseFloat(s27);
					t27.setBackgroundColor(Color.TRANSPARENT);


				}
				String s28 = t28.getText().toString();
				if(s28.compareTo("")==0){


					t28.setBackgroundColor(Color.RED);
				}
				else{

					matrix[1][7] = Float.parseFloat(s28);
					t28.setBackgroundColor(Color.TRANSPARENT);


				}
				String s29 = t29.getText().toString();
				if(s29.compareTo("")==0){


					t29.setBackgroundColor(Color.RED);
				}
				else{

					matrix[1][8] = Float.parseFloat(s29);
					t29.setBackgroundColor(Color.TRANSPARENT);


				}



				String s31 = t31.getText().toString();
				if(s31.compareTo("")==0){


					t31.setBackgroundColor(Color.RED);
				}
				else{

					matrix[2][0] = Float.parseFloat(s31);
					t31.setBackgroundColor(Color.TRANSPARENT);


				}

				String s32 = t32.getText().toString();
				if(s32.compareTo("")==0){


					t32.setBackgroundColor(Color.RED);
				}
				else{

					matrix[2][1] = Float.parseFloat(s32);
					t32.setBackgroundColor(Color.TRANSPARENT);


				}

				String s33 = t33.getText().toString();
				if(s33.compareTo("")==0){


					t33.setBackgroundColor(Color.RED);
				}
				else{

					matrix[2][2] = Float.parseFloat(s33);
					t33.setBackgroundColor(Color.TRANSPARENT);


				}

				String s34 = t34.getText().toString();
				if(s34.compareTo("")==0){


					t34.setBackgroundColor(Color.RED);
				}
				else{

					matrix[2][3] = Float.parseFloat(s34);
					t34.setBackgroundColor(Color.TRANSPARENT);


				}
				String s35 = t35.getText().toString();
				if(s35.compareTo("")==0){


					t35.setBackgroundColor(Color.RED);
				}
				else{

					matrix[2][4] = Float.parseFloat(s35);
					t35.setBackgroundColor(Color.TRANSPARENT);


				}
				String s36 = t36.getText().toString();
				if(s36.compareTo("")==0){


					t36.setBackgroundColor(Color.RED);
				}
				else{

					matrix[2][5] = Float.parseFloat(s36);
					t36.setBackgroundColor(Color.TRANSPARENT);


				}
				String s37 = t37.getText().toString();
				if(s37.compareTo("")==0){


					t37.setBackgroundColor(Color.RED);
				}
				else{

					matrix[2][6] = Float.parseFloat(s37);
					t37.setBackgroundColor(Color.TRANSPARENT);


				}
				String s38 = t38.getText().toString();
				if(s38.compareTo("")==0){


					t38.setBackgroundColor(Color.RED);
				}
				else{

					matrix[2][7] = Float.parseFloat(s38);
					t38.setBackgroundColor(Color.TRANSPARENT);


				}
				String s39 = t39.getText().toString();
				if(s39.compareTo("")==0){


					t39.setBackgroundColor(Color.RED);
				}
				else{

					matrix[2][8] = Float.parseFloat(s39);
					t39.setBackgroundColor(Color.TRANSPARENT);


				}

				String s41 = t41.getText().toString();
				if(s41.compareTo("")==0){


					t41.setBackgroundColor(Color.RED);
				}
				else{

					matrix[3][0] = Float.parseFloat(s41);
					t41.setBackgroundColor(Color.TRANSPARENT);


				}

				String s42 = t42.getText().toString();
				if(s42.compareTo("")==0){


					t42.setBackgroundColor(Color.RED);
				}
				else{

					matrix[3][1] = Float.parseFloat(s42);
					t42.setBackgroundColor(Color.TRANSPARENT);


				}

				String s43 = t43.getText().toString();
				if(s43.compareTo("")==0){


					t43.setBackgroundColor(Color.RED);
				}
				else{

					matrix[3][2] = Float.parseFloat(s43);
					t43.setBackgroundColor(Color.TRANSPARENT);


				}

				String s44 = t44.getText().toString();
				if(s44.compareTo("")==0){


					t44.setBackgroundColor(Color.RED);
				}
				else{

					matrix[3][3] = Float.parseFloat(s44);
					t44.setBackgroundColor(Color.TRANSPARENT);


				}
				String s45 = t45.getText().toString();
				if(s45.compareTo("")==0){


					t45.setBackgroundColor(Color.RED);
				}
				else{

					matrix[3][4] = Float.parseFloat(s45);
					t45.setBackgroundColor(Color.TRANSPARENT);


				}
				String s46 = t46.getText().toString();
				if(s46.compareTo("")==0){


					t46.setBackgroundColor(Color.RED);
				}
				else{

					matrix[3][5] = Float.parseFloat(s46);
					t46.setBackgroundColor(Color.TRANSPARENT);


				}
				String s47 = t47.getText().toString();
				if(s47.compareTo("")==0){


					t47.setBackgroundColor(Color.RED);
				}
				else{

					matrix[3][6] = Float.parseFloat(s47);
					t47.setBackgroundColor(Color.TRANSPARENT);


				}
				String s48 = t48.getText().toString();
				if(s48.compareTo("")==0){


					t48.setBackgroundColor(Color.RED);
				}
				else{

					matrix[3][7] = Float.parseFloat(s48);
					t48.setBackgroundColor(Color.TRANSPARENT);


				}
				String s49 = t49.getText().toString();
				if(s49.compareTo("")==0){


					t49.setBackgroundColor(Color.RED);
				}
				else{

					matrix[3][8] = Float.parseFloat(s49);
					t49.setBackgroundColor(Color.TRANSPARENT);


				}

				String s51 = t51.getText().toString();
				if(s51.compareTo("")==0){


					t51.setBackgroundColor(Color.RED);
				}
				else{

					matrix[4][0] = Float.parseFloat(s51);
					t51.setBackgroundColor(Color.TRANSPARENT);


				}

				String s52 = t52.getText().toString();
				if(s52.compareTo("")==0){


					t52.setBackgroundColor(Color.RED);
				}
				else{

					matrix[4][1] = Float.parseFloat(s52);
					t52.setBackgroundColor(Color.TRANSPARENT);


				}

				String s53 = t53.getText().toString();
				if(s53.compareTo("")==0){


					t53.setBackgroundColor(Color.RED);
				}
				else{

					matrix[4][2] = Float.parseFloat(s53);
					t53.setBackgroundColor(Color.TRANSPARENT);


				}

				String s54 = t54.getText().toString();
				if(s54.compareTo("")==0){


					t54.setBackgroundColor(Color.RED);
				}
				else{

					matrix[4][3] = Float.parseFloat(s54);
					t54.setBackgroundColor(Color.TRANSPARENT);


				}
				String s55 = t55.getText().toString();
				if(s55.compareTo("")==0){


					t55.setBackgroundColor(Color.RED);
				}
				else{

					matrix[4][4] = Float.parseFloat(s55);
					t55.setBackgroundColor(Color.TRANSPARENT);


				}
				String s56 = t56.getText().toString();
				if(s56.compareTo("")==0){


					t56.setBackgroundColor(Color.RED);
				}
				else{

					matrix[4][5] = Float.parseFloat(s56);
					t56.setBackgroundColor(Color.TRANSPARENT);


				}
				String s57 = t57.getText().toString();
				if(s57.compareTo("")==0){


					t57.setBackgroundColor(Color.RED);

				}
				else{

					matrix[4][6] = Float.parseFloat(s57);
					t57.setBackgroundColor(Color.TRANSPARENT);


				}
				String s58 = t58.getText().toString();
				if(s58.compareTo("")==0){


					t58.setBackgroundColor(Color.RED);
				}
				else{

					matrix[4][7] = Float.parseFloat(s58);
					t58.setBackgroundColor(Color.TRANSPARENT);


				}
				String s59 = t59.getText().toString();
				if(s59.compareTo("")==0){


					t59.setBackgroundColor(Color.RED);
				}
				else{

					matrix[4][8] = Float.parseFloat(s59);
					t59.setBackgroundColor(Color.TRANSPARENT);


				}


				String s61 = t61.getText().toString();
				if(s61.compareTo("")==0){


					t61.setBackgroundColor(Color.RED);
				}
				else{

					matrix[5][0] = Float.parseFloat(s61);
					t61.setBackgroundColor(Color.TRANSPARENT);


				}

				String s62 = t62.getText().toString();
				if(s62.compareTo("")==0){


					t62.setBackgroundColor(Color.RED);
				}
				else{

					matrix[5][1] = Float.parseFloat(s62);
					t62.setBackgroundColor(Color.TRANSPARENT);


				}

				String s63 = t63.getText().toString();
				if(s63.compareTo("")==0){


					t63.setBackgroundColor(Color.RED);
				}
				else{

					matrix[5][2] = Float.parseFloat(s63);
					t63.setBackgroundColor(Color.TRANSPARENT);


				}

				String s64 = t64.getText().toString();
				if(s64.compareTo("")==0){


					t64.setBackgroundColor(Color.RED);
				}
				else{

					matrix[5][3] = Float.parseFloat(s64);
					t64.setBackgroundColor(Color.TRANSPARENT);


				}
				String s65 = t65.getText().toString();
				if(s65.compareTo("")==0){


					t65.setBackgroundColor(Color.RED);
				}
				else{

					matrix[5][4] = Float.parseFloat(s65);
					t65.setBackgroundColor(Color.TRANSPARENT);


				}
				String s66 = t66.getText().toString();
				if(s66.compareTo("")==0){


					t66.setBackgroundColor(Color.RED);

				}
				else{

					matrix[5][5] = Float.parseFloat(s66);
					t66.setBackgroundColor(Color.TRANSPARENT);


				}
				String s67 = t67.getText().toString();
				if(s67.compareTo("")==0){


					t67.setBackgroundColor(Color.RED);
				}
				else{

					matrix[5][6] = Float.parseFloat(s67);
					t67.setBackgroundColor(Color.TRANSPARENT);


				}
				String s68 = t68.getText().toString();
				if(s68.compareTo("")==0){


					t68.setBackgroundColor(Color.RED);
				}
				else{

					matrix[5][7] = Float.parseFloat(s68);
					t68.setBackgroundColor(Color.TRANSPARENT);


				}
				String s69 = t69.getText().toString();
				if(s69.compareTo("")==0){


					t69.setBackgroundColor(Color.RED);
				}
				else{

					matrix[5][8] = Float.parseFloat(s69);
					t69.setBackgroundColor(Color.TRANSPARENT);


				}

				String s71 = t71.getText().toString();
				if(s71.compareTo("")==0){


					t71.setBackgroundColor(Color.RED);
				}
				else{

					matrix[6][0] = Float.parseFloat(s71);
					t71.setBackgroundColor(Color.TRANSPARENT);


				}

				String s72 = t72.getText().toString();
				if(s72.compareTo("")==0){


					t72.setBackgroundColor(Color.RED);
				}
				else{

					matrix[6][1] = Float.parseFloat(s72);
					t72.setBackgroundColor(Color.TRANSPARENT);


				}

				String s73 = t73.getText().toString();
				if(s73.compareTo("")==0){


					t73.setBackgroundColor(Color.RED);
				}
				else{

					matrix[6][2] = Float.parseFloat(s73);
					t73.setBackgroundColor(Color.TRANSPARENT);


				}

				String s74 = t74.getText().toString();
				if(s74.compareTo("")==0){


					t74.setBackgroundColor(Color.RED);
				}
				else{

					matrix[6][3] = Float.parseFloat(s74);
					t74.setBackgroundColor(Color.TRANSPARENT);


				}
				String s75 = t75.getText().toString();
				if(s75.compareTo("")==0){


					t75.setBackgroundColor(Color.RED);
				}
				else{

					matrix[6][4] = Float.parseFloat(s75);
					t75.setBackgroundColor(Color.TRANSPARENT);


				}
				String s76 = t76.getText().toString();
				if(s76.compareTo("")==0){


					t76.setBackgroundColor(Color.RED);
				}
				else{

					matrix[6][5] = Float.parseFloat(s76);
					t76.setBackgroundColor(Color.TRANSPARENT);


				}
				String s77 = t77.getText().toString();
				if(s77.compareTo("")==0){


					t77.setBackgroundColor(Color.RED);
				}
				else{

					matrix[6][6] = Float.parseFloat(s77);
					t77.setBackgroundColor(Color.TRANSPARENT);


				}
				String s78 = t78.getText().toString();
				if(s78.compareTo("")==0){


					t78.setBackgroundColor(Color.RED);
				}
				else{

					matrix[6][7] = Float.parseFloat(s78);
					t78.setBackgroundColor(Color.TRANSPARENT);


				}
				String s79 = t79.getText().toString();
				if(s79.compareTo("")==0){


					t79.setBackgroundColor(Color.RED);
				}
				else{

					matrix[6][8] = Float.parseFloat(s79);
					t79.setBackgroundColor(Color.TRANSPARENT);


				}


				String s81 = t81.getText().toString();
				if(s81.compareTo("")==0){


					t81.setBackgroundColor(Color.RED);
				}
				else{

					matrix[7][0] = Float.parseFloat(s81);
					t81.setBackgroundColor(Color.TRANSPARENT);


				}

				String s82 = t82.getText().toString();
				if(s82.compareTo("")==0){


					t82.setBackgroundColor(Color.RED);
				}
				else{

					matrix[7][1] = Float.parseFloat(s82);
					t82.setBackgroundColor(Color.TRANSPARENT);


				}

				String s83 = t83.getText().toString();
				if(s83.compareTo("")==0){


					t83.setBackgroundColor(Color.RED);
				}
				else{

					matrix[7][2] = Float.parseFloat(s83);
					t83.setBackgroundColor(Color.TRANSPARENT);


				}

				String s84 = t84.getText().toString();
				if(s84.compareTo("")==0){


					t84.setBackgroundColor(Color.RED);
				}
				else{

					matrix[7][3] = Float.parseFloat(s84);
					t84.setBackgroundColor(Color.TRANSPARENT);


				}
				String s85 = t85.getText().toString();
				if(s85.compareTo("")==0){


					t85.setBackgroundColor(Color.RED);
				}
				else{

					matrix[7][4] = Float.parseFloat(s85);
					t85.setBackgroundColor(Color.TRANSPARENT);


				}
				String s86 = t86.getText().toString();
				if(s86.compareTo("")==0){


					t86.setBackgroundColor(Color.RED);
				}
				else{

					matrix[7][5] = Float.parseFloat(s86);
					t86.setBackgroundColor(Color.TRANSPARENT);


				}
				String s87 = t87.getText().toString();
				if(s87.compareTo("")==0){


					t87.setBackgroundColor(Color.RED);
				}
				else{

					matrix[7][6] = Float.parseFloat(s87);
					t87.setBackgroundColor(Color.TRANSPARENT);


				}
				String s88 = t88.getText().toString();
				if(s88.compareTo("")==0){


					t88.setBackgroundColor(Color.RED);
				}
				else{

					matrix[7][7] = Float.parseFloat(s88);
					t88.setBackgroundColor(Color.TRANSPARENT);


				}
				String s89 = t89.getText().toString();
				if(s89.compareTo("")==0){


					t89.setBackgroundColor(Color.RED);
				}
				else{

					matrix[7][8] = Float.parseFloat(s89);
					t89.setBackgroundColor(Color.TRANSPARENT);


				}

				String s91 = t91.getText().toString();
				if(s91.compareTo("")==0){


					t91.setBackgroundColor(Color.RED);
				}
				else{

					matrix[8][0] = Float.parseFloat(s91);
					t91.setBackgroundColor(Color.TRANSPARENT);


				}

				String s92 = t92.getText().toString();
				if(s92.compareTo("")==0){


					t92.setBackgroundColor(Color.RED);
				}
				else{

					matrix[8][1] = Float.parseFloat(s92);
					t92.setBackgroundColor(Color.TRANSPARENT);


				}

				String s93 = t93.getText().toString();
				if(s93.compareTo("")==0){


					t93.setBackgroundColor(Color.RED);
				}
				else{

					matrix[8][2] = Float.parseFloat(s93);
					t93.setBackgroundColor(Color.TRANSPARENT);


				}

				String s94 = t94.getText().toString();
				if(s94.compareTo("")==0){


					t94.setBackgroundColor(Color.RED);
				}
				else{

					matrix[8][3] = Float.parseFloat(s94);
					t94.setBackgroundColor(Color.TRANSPARENT);


				}
				String s95 = t95.getText().toString();
				if(s95.compareTo("")==0){


					t95.setBackgroundColor(Color.RED);
				}
				else{

					matrix[8][4] = Float.parseFloat(s95);
					t95.setBackgroundColor(Color.TRANSPARENT);


				}
				String s96 = t96.getText().toString();
				if(s96.compareTo("")==0){


					t96.setBackgroundColor(Color.RED);
				}
				else{

					matrix[8][5] = Float.parseFloat(s96);
					t96.setBackgroundColor(Color.TRANSPARENT);


				}
				String s97 = t97.getText().toString();
				if(s97.compareTo("")==0){


					t97.setBackgroundColor(Color.RED);
				}
				else{

					matrix[8][6] = Float.parseFloat(s97);
					t97.setBackgroundColor(Color.TRANSPARENT);


				}
				String s98 = t98.getText().toString();
				if(s98.compareTo("")==0){


					t98.setBackgroundColor(Color.RED);
				}
				else{

					matrix[8][7] = Float.parseFloat(s98);
					t98.setBackgroundColor(Color.TRANSPARENT);


				}
				String s99 = t99.getText().toString();
				if(s99.compareTo("")==0){


					t99.setBackgroundColor(Color.RED);
				}
				else{

					matrix[8][8] = Float.parseFloat(s99);
					t99.setBackgroundColor(Color.TRANSPARENT);


				}
				if(matrizeaEgiaztatu(MainActivity.m, MainActivity.n)){

					metodoak1.matrizeaInprimatu(matrix, MainActivity.m, MainActivity.n);		
					Intent hurren= new Intent(this, MainActivity3.class);
					startActivity(hurren);
				}

			}
		
		return super.onOptionsItemSelected(item);

	}

}
