package com.gaus;


import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.Scanner;

import org.w3c.dom.ls.LSInput;


public class metodoak1 {

	static LinkedList<float[][]> lista = new LinkedList<float[][]>();

	public static LinkedList<float[][]> gauss(int m, int n, float[][] matrizea) throws InterruptedException
	{


		matrizeaInprimatu(matrizea, m, n);
		listaraGehitu(matrizea);
		float biderkagaia=0;
		for(int l=0; l<n-1 && l<m-1; l++)
		{

			System.out.println("......................................................");

			aldaketaBAIoEZ(matrizea, m, n);
			int ezabErren=errenkadaZeroz(matrizea, m, n);
			if(ezabErren!=-1)
			{
				boolean ezabatuta=false;
				float[][] matrizeaLag=new float[m][n];
				matrizeaLag=matrizea;
				matrizea=new float[m-1][n];
				for(int i=0; i<m; i++)
				{
					if(i!=ezabErren && ezabatuta==false)
					{
						for(int j=0; j<n; j++)
						{
							matrizea[i][j]=matrizeaLag[i][j];
						}

					}
					else if(ezabatuta==true)
					{
						for(int j=0; j<n; j++)
						{
							matrizea[i-1][j]=matrizeaLag[i][j];
						}

					}
					else if(i==ezabErren)ezabatuta=true;
				}
				m=m-1;
				MainActivity.m-=1;

				System.out.println();
				System.out.println();
				matrizeaInprimatu(matrizeaLag, m, ezabErren);
				listaraGehitu(matrizea);
				System.out.println("......................................................");


			}
			for(int j=l+1; j<m; j++)
			{
				//Thread.sleep(500);
				if(matrizea[j][l]!=0)
				{
					biderkagaia=matrizea[j][l]/matrizea[l][l];
					for(int i=0; i<n; i++)
						matrizea[j][i]=matrizea[j][i]-(biderkagaia*matrizea[l][i]);
				}
				System.out.println();
				System.out.println("");
				System.out.println();
				System.out.println("......................................................");
				matrizeaInprimatu(matrizea, m, n);
				listaraGehitu(matrizea);

			}
			System.out.println();
			System.out.println();

		}

		return lista;
	}

	public static void matrizeaInprimatu(float[][] matrizea, int m, int n)
	{
		for (int i = 0; i < m; i++) 
		{
			System.out.print("     ");
			for (int j = 0; j < n; j++) 
			{
				if(matrizea[i][j]%1!=0)
				{
					System.out.print(" ** ");
					zatikiraPasa(matrizea[i][j]);
				}
				else if(matrizea[i][j]==0)
					System.out.print("  **  "+(int)matrizea[i][j]);
				else
					if(matrizea[i][j]<0)
						System.out.print("  **  "+(int)matrizea[i][j]);
					else System.out.print("  **  "+(int)matrizea[i][j]);

			}
			System.out.println(" ** ");
		}
	}
	public static String zatikiraPasa(float f)
	{
		float lag=f;
		DecimalFormat df = new DecimalFormat("0.0");

		//		int zatitzailea=1;
		//		while(f%1!=0)
		//		{
		//			f=f*10;
		//			zatitzailea=zatitzailea*10;
		//		}
		//		for(int j=0; j<10; j++)
		//			for(int i=2; i<10; i++)
		//				if(f%i==0 && zatitzailea%i==0)
		//				{
		//					f=f/i;
		//					zatitzailea=zatitzailea/i;
		//				}
		//		if(df.format(lag).compareTo("3,33")==0)return "10/3";
		//		else if(df.format(lag).compareTo("-0,00")==0||df.format(lag).compareTo("0,00")==0)return "0";
		//		else if(zatitzailea<100 && zatitzailea>-100){
		//			
		//			System.out.print((int)f+"/"+zatitzailea);
		//			return String.valueOf((int)f)+"/"+String.valueOf(zatitzailea);
		//		}
		//		else{

		System.out.print(df.format(lag));
		return String.valueOf(df.format(lag));

	}

	private static void aldaketaBAIoEZ(float[][] matrizea, int m, int n)
	{
		int ald1=0;
		int ald2=0;
		boolean aldatu=false;

		for(int i=0; i<m; i++)
		{
			for(int j=0; j<n; j++)
			{
				if(matrizea[i][j]==0 && i==j)
				{
					ald1=i;//0-a duena.
					ald2=i+1;
					aldatu=true;
					break;
				}
			}
			if(aldatu==true)break;
		}
		//System.out.println(ald1+"-------"+ald2);

		if(aldatu==true)//array-ekin kontuz ibili behar da... helbidea pasatzen baizaie.
		{
			float[][] lag=new float[m][n];	
			for(int i=0; i<m; i++)
			{
				for(int j=0; j<n; j++)
				{
					lag[i][j]=matrizea[i][j];
				}
			}
			System.out.println();
			for(int i=0; i<n; i++)matrizea[ald1][i]=lag[ald2][i];
			for(int i=0; i<n; i++)matrizea[ald2][i]=lag[ald1][i];
			listaraGehitu(matrizea);
			System.out.println();
			System.out.println("......................................................");

		}
	}

	private static int errenkadaZeroz(float[][] matrizea, int m, int n)
	//-1 itzuli ez badago
	//bestela ezabatu beharreko errenkada(obeto pentsau)
	{
		int ezabatzeko=-1;
		for(int i=0; i<m; i++)
		{
			ezabatzeko=i;
			for(int j=0; j<n; j++)
			{

				if(matrizea[i][j]!=0)ezabatzeko=-1;				
			}
		}
		return ezabatzeko;
	}

	private static void listaraGehitu(float[][] matrix){

		float[][] lag=new float[9][9];	
		for(int i=0; i<9; i++)
		{
			for(int j=0; j<9; j++)
			{
				lag[i][j]=matrix[i][j];
			}
		}
		lista.add(lag);

	}
}
