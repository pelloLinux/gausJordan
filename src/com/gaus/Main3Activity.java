package com.gaus;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

public class Main3Activity extends ActionBarActivity implements OnClickListener{

	static int indexLagun;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main3);
		
		indexLagun = MainActivity3.index;
		MainActivity3.pasauta = true;

		TextView text_m=(TextView) findViewById(R.id.textView1);
		text_m.setText("Write coordinates,                     n[0.."+(MainActivity.n-1) +"] & m[0.."+(MainActivity.m-1)+"]");

		TextView b=(TextView) findViewById(R.id.button1);
		b.setOnClickListener(this);

		TextView b2=(TextView) findViewById(R.id.button2);
		b2.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub


		if(v.getId()==findViewById(R.id.button1).getId()){

			TextView text_num=(TextView) findViewById(R.id.textView4);
			float[][] matrix=MainActivity3.lista2.get(MainActivity3.index);

			EditText n=(EditText)findViewById(R.id.editText1);
			EditText m=(EditText)findViewById(R.id.editText2);

			String n_string = n.getText().toString();
			String m_string = m.getText().toString();

			int n_int = Integer.parseInt(n_string);
			int m_int = Integer.parseInt(m_string);

			if(n_int <= MainActivity.n && m_int <= MainActivity.m){

				float num_float = matrix[n_int][m_int];
				String num_String = String.valueOf(num_float);

				text_num.setText(num_String);

			}

		}else{
			
			Intent hurren= new Intent(this, MainActivity4.class);
			startActivity(hurren);
		}

	}


}
